package method;

public class Demo03 {
    public int add(int a,int b){
        return a+b;
    }

    public double add(double a,double b){
        return a+b;
    }

    public int add(int a,int b,int c){
        return a+b+c;
    }

    public static void main(String[] args) {
        Demo03 demo03 = new Demo03();
        System.out.println(demo03.add(10,20));
        System.out.println(demo03.add(1.1,2.2));
        System.out.println(demo03.add(1,2,3));
    }
}
