package method;

import javax.swing.plaf.nimbus.AbstractRegionPainter;

public class Demo05 {
    public static void main(String[] args) {
        Demo05 demo05 = new Demo05();
        System.out.println(demo05.sum(10));
    }
    public int sum(int a){
        if (a==1){
            return 1;
        }else {
            return a+sum(--a);
        }
    }
}
