package method;

public class Demo04 {
    public static void main(String[] args) {
        Demo04 demo04 = new Demo04();
        System.out.println(demo04.add(1,2,3,4));
    }

    public int add(int b,int ...a){
        for (int i = 0; i < a.length; i++) {
            b+=a[i];
        }
        return b;
    }
}
