package method;

public class Demo02 {
    public static void main(String[] args) {
        Demo02 demo02 = new Demo02();
        int sum=demo02.add(10,20);
        System.out.println(sum);
        System.out.println(demo02.max(10,10));
    }

    public int add(int a,int b){
        return a+b;
    }

    public int max(int num1,int num2){
        if (num1>num2){
            return num1;
        }else if (num2>num1){
            return num2;
        }else {
            System.out.println("相等");
            return 0;
        }
    }
}
