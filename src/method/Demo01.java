package method;

public class Demo01 {
    //main方法
    public static void main(String[] args) {
        int x=10;
        int y=20;
        Demo01 d=new Demo01();
        System.out.println(d.add(x,y));

        System.out.println(bidaxiao(10,20));
        d.test(1,2,3,4);
        System.out.println(d.digui(5));
    }

    //加方法
    public int add(int a,int b){
        return a+b;
    }

    public static int add1(int a,int b){
        return a+b;
    }

    //比大小
    public static int bidaxiao(int a,int b){

        if (a==b){
            return 0;//return可以用于终止方法。
        }

        if (a>b){
            return a;
        }else {
            return b;
        }
    }

    public static double bidaxiao(double a,double b){

        if (a==b){
            return 0;//return可以用于终止方法。
        }

        if (a>b){
            return a;
        }else {
            return b;
        }
    }

    public void test(int ...a){
        System.out.println(a[0]);
    }

    //递归
    public int digui(int a){
        if (a==1){
            return 1;
        }else {
            return a*digui(a-1);
        }
    }



}
