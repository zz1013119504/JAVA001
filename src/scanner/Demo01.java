package scanner;

import java.awt.*;
import java.util.Scanner;

public class Demo01 {
    //通过Scanner类的next()和nextLine()方法获取输入的字符串，在读取前我们一般需要
    //使用hasNext()与hasNextLine()判断是否还有输入的数据。
    public static void main(String[] args) {
        //创建一个扫描对象，用于接收键盘输入
        Scanner scanner =new Scanner(System.in);

        System.out.println("使用next方法来接收数据：");

//        //判断用户是否输入数据
//        if (scanner.hasNext()){
//            //使用next方法接收数据
//            String str=scanner.next();
//            System.out.println("输出的内容为："+str);
//        }

        if (scanner.hasNextLine()){
            String s=scanner.nextLine();
            System.out.println("输出内容是："+s);
        }


        Scanner scanner1=new Scanner(System.in);
        System.out.println("请输入数据：");

        int i=0;
        Float f=0.0f;

        if (scanner1.hasNextInt()){
            i=scanner1.nextInt();
            System.out.println("整数是："+i);
        }else if (scanner1.hasNextFloat()){
            f=scanner1.nextFloat();
            System.out.println("输入数据为小数："+f);
        }else {
            System.out.println("输入数据不是整数也不是小数！！！");
        }


        //输入多个数字，并求其和与平均数，通过非数字形式结束。
        Scanner scanner2=new Scanner(System.in);

        //和
        double sum=0;
        //计算输入了多少次
        int m=0;
        //通过循环判断输入是否存在，并求其每一次输入之后的和。
        while (scanner2.hasNextDouble()){
            sum=sum+scanner2.nextDouble();
            m=m+1;
            System.out.println("你输入了第"+m+"个数，当前的和为："+sum);
        }
        System.out.println("平均数为："+sum/m);

        //凡是属于IO流的类如果不关闭，就会一直占用资源，养成良好的用完即关闭习惯
        scanner.close();
    }
}
