package thread;

//模拟延时：放大网络问题的发生性
public class TestSleep implements Runnable{

    //票数
    private int ticketnums=10;


    @Override
    public void run() {
    while (true){
        if (ticketnums<=0){
            break;
        }
        //模拟延时
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName()+"拿到了第"+ticketnums--+"张票");
    }
    }

    public static void main(String[] args) {
        TestSleep testSleep = new TestSleep();
        new Thread(testSleep,"小明").start();
        new Thread(testSleep,"小红").start();
        new Thread(testSleep,"黄牛").start();
    }
}
