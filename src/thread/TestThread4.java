package thread;

import javax.swing.*;
import java.util.concurrent.locks.ReentrantLock;

public class TestThread4 implements Runnable {

    //票数
    private int num = 10;

    //定义lock锁
    private final ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true) {
            try {
                lock.lock();
                //模拟延时
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (num == 0) {
                    break;
                }

                //Thread.currentThread().getName()：拿到当前线程的名字
                System.out.println(Thread.currentThread().getName() + "拿到了第" + num-- + "张票");

            } finally {
                //解锁
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        TestThread4 testThread4 = new TestThread4();

        new Thread(testThread4, "小明").start();
        new Thread(testThread4, "小红").start();
        new Thread(testThread4, "黄牛").start();
    }
}

