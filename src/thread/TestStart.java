package thread;

public class TestStart {
    public static void main(String[] args) {
        //main线程，主线程

        //创建一个线程对象
        TestThread1 testThread1 = new TestThread1();

        //调用start方法
        testThread1.start();

        for (int i = 0; i < 200; i++) {
            System.out.println("我在学习另一个"+i);
        }
    }
}
