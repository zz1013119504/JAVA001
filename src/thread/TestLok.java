package thread;

import java.util.concurrent.locks.ReentrantLock;

public class TestLok {
    public static void main(String[] args) {
        TestLock2 testLock2 = new TestLock2();

        new Thread(testLock2,"小红").start();
        new Thread(testLock2,"小兰").start();
        new Thread(testLock2,"黄牛").start();
    }

}

class TestLock2 implements Runnable{
    private int ticknums=10;
    boolean flag=true;

    //定义Lock锁
    private final ReentrantLock lock=new ReentrantLock();

    @Override
    public void run() {
        while (flag){
            try {
                lock.lock();
                if (ticknums <= 0) {
                    flag = false;
                    break;
                }
                System.out.println(Thread.currentThread().getName() + ticknums--);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }finally {
                lock.unlock();
            }
        }
    }
}
