package thread;

//创建线程方式1：继承Thread类，重写run方法，调用start开启线程
//总结：注意，线程开启不一定立即执行，由CPU调度执行。
public class TestThread1 extends Thread {
    @Override
    public void run() {
        //run方法线程体
        for (int i = 0; i < 20; i++) {
            System.out.println("我已经学了"+i+"遍了");
        }
    }

}
