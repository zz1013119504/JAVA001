package thread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

//练习Thread，实现多线程同步下载图片
public class TestThread2 extends Thread{
    private String url;//网络图片地址
    private String name;//保存的文件名

    public TestThread2(String url, String name) {
        this.url = url;
        this.name = name;
    }

    //下载图片线程的执行体
    @Override
    public void run() {
        WebDownLoader webDownLoader = new WebDownLoader();
        webDownLoader.downloader(url,name);
        System.out.println("下载了文件名为："+name);
    }

    public static void main(String[] args) {
        TestThread2 t1 = new TestThread2("https://img.yalayi.net/d/file/2020/07/02/56acd168bcfa628357171e2238fc706f.jpg", "1.jpg");
        TestThread2 t2 = new TestThread2("https://img.yalayi.net/d/file/2020/07/02/683b5c42ff95dabf391fe6206a517bb7.jpg", "2.jpg");
        TestThread2 t3 = new TestThread2("https://img.yalayi.net/d/file/2020/07/02/3684e97a6c2ed42119f4ae9d6185261d.jpg", "3.jpg");

        t1.start();
        t2.start();
        t3.start();
    }
}

//下载器
class WebDownLoader{
    //下载方法
    public void downloader(String url,String name){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常！");
        }
    }
}
