package thread;


//代理对象的好处：1.代理对象可以做很多真实对象做不了的事情。2.真实对象专注做自己的事情。
public class StacticProxy {
    public static void main(String[] args) {
        WeddingCompany weddingCompany = new WeddingCompany(new You());
        weddingCompany.HappyMarry();
    }
}

interface Marry{

    void HappyMarry();
}

//真实角色，主角
class You implements Marry{

    @Override
    public void HappyMarry() {
        System.out.println("结婚了，好开心啊");
    }
}

//代理角色，帮助操办结婚的
class WeddingCompany implements Marry{

    //代理谁——》真实目标角色
    private Marry targrt;

    public WeddingCompany(Marry targrt) {
        this.targrt = targrt;
    }

    @Override
    public void HappyMarry() {
        
        before();
        this.targrt.HappyMarry();//这就是真实对象
        after();
    }

    private void after() {
        System.out.println("结婚之后，收尾款");
    }

    private void before() {
        System.out.println("结婚之前，布置现场");
    }
}
