package thread;

import java.util.concurrent.locks.ReentrantLock;

public class UnsafeBuyTicket {
    public static void main(String[] args) {
        BuyTicket buyTicket = new BuyTicket();

        new Thread(buyTicket,"小红").start();
        new Thread(buyTicket,"小，名").start();
        new Thread(buyTicket,"黄牛").start();
    }
}

class BuyTicket implements Runnable{

    //票
    private int ticketnums=10;
    boolean flag=true;
    ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (flag){

            buy();

        }
    }

    public synchronized void buy(){


        if (ticketnums<=0){
            this.flag=false;
            return;
        }

        {
            try {

                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"获得第"+ticketnums--);
        }

    }

}
