package thread;

public class demostop implements Runnable {

    private boolean flag = true;

    @Override
    public void run() {
        int i = 0;
        while (flag) {
            System.out.println(i++);
        }

    }

    public void stop() {
        this.flag = false;
    }

    public static void main(String[] args) {
        demostop demostop = new demostop();

        new Thread(demostop).start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("i******************" + i);
            if (i == 900) {
                demostop.stop();
                System.out.println("停止线程！" + i);
            }
        }
    }
}
