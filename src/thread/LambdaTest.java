package thread;

public class LambdaTest {

    public static void main(String[] args) {
        Love love=a->System.out.println(a*12);
        love.love(521);
    }
}

interface Love{
    void love(int a);
}
