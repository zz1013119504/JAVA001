package thread;

public class TestDaemon {

    public static void main(String[] args) {
        God god = new God();
        Youu youu = new Youu();

        //将god设置为守护线程
        Thread thread = new Thread(god);
        thread.setDaemon(true);
        thread.start();

        new Thread(youu).start();

    }
}

class God implements Runnable{
    @Override
    public void run() {
        while (true){
        System.out.println("死神永生");
    }}
}

class Youu implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 36500; i++) {
            System.out.println("一直都快乐的货品这");
        }
        System.out.println("哦豁，洗白了");
    }
}
