package jdbc;

import java.sql.*;

//创建第一个JDBC程序
public class JdbcFirstDemo01 {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //1. 加载驱动
        Class.forName("com.mysql.jdbc.Driver");//固定写法，加载驱动

        //2. 用户信息和url
        String url="jdbc:mysql://localhost:3306/dongruan?useUnicode=true&characterEncoding=utf8&useSSL=true";
        String username="root";
        String password="lunhui555&*";

        //3. 链接成功，数据库对象 Connection 代表数据库
        Connection connection = DriverManager.getConnection(url, username, password);

        //4. 执行SQL的对象 Statement执行sql的对象
        Statement statement = connection.createStatement();

        //5. 执行SQL的对象去执行SQL，可能存在结果，查看返回结果
        String sql="show tables";
        ResultSet resultSet = statement.executeQuery(sql);//返回结果集，结果集中封装了我们全部的查询结果

        while (resultSet.next()){
            System.out.println(resultSet.getString(1));
        }

        //6. 释放链接
        resultSet.close();
        statement.close();
        connection.close();
    }
}
