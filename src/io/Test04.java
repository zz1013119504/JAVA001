package io;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Test04 {
    public static void main(String[] args) {
        //创建对象流
        try {
            FileOutputStream fi = new FileOutputStream("ddd.txt");
            ObjectOutputStream bos = new ObjectOutputStream(fi);

            //序列化（写入操作）
            Student zhangsan = new Student("战三", 18);
            bos.writeObject(zhangsan);
            bos.flush();

            //关闭
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

        }

    }
}
