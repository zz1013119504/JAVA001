package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo1 {
    public static void main(String[] args) {
        //1创建FileInputStream，并制定文件路劲
        try {
            FileInputStream fis = new FileInputStream("D:\\PyCharmProject\\JAVA\\D001\\src\\io\\aaa.txt");
            //2读取文件
            int data=0;
            while ((data=fis.read())!=-1){
                System.out.print(((char)data));
            }

            //关闭输入流
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
