package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Test02 {

    public static void main(String[] args) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("bbb.txt");

            String a="hello world";

            fileOutputStream.write(a.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
