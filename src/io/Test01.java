package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Test01 {

    public static void main(String[] args) {
        try {
            FileInputStream fileInputStream = new FileInputStream("aaa.txt");

            //一次读取多个字节。首先创建一个数组，数组的大小决定每次读取的字节大小
            byte[] bytes = new byte[3];
            int count=0;
            while ((count=fileInputStream.read(bytes))!=-1){
                System.out.println(new String(bytes,0,count));
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
