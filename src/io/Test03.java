package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Test03 {

    public static void main(String[] args) {


        try {
            //输入流
            FileInputStream fileInputStream = new FileInputStream("aaa.txt");
            //输出流
            FileOutputStream fileOutputStream = new FileOutputStream("ccc.txt");

            byte[] bytes = new byte[10];
            int count=0;

            while ((count=fileInputStream.read(bytes))!=-1){
                fileOutputStream.write(bytes,0,count);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
