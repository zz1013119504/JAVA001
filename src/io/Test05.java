package io;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Test05 {
    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("aaa.txt");

            char[] ch = new char[1024];
            int count=0;
            while ((count=fileReader.read(ch))!=-1){
                System.out.println(new String(ch,0,count));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
