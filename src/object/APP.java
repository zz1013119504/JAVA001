package object;

public class APP {
    public static void main(String[] args) {

        //使用泛型类创建对象
        //注意：1.泛型只能使用引用类型。2.不同泛型对象之间不能相互复制。
        Demo2<String> objectDemo2 = new Demo2<>();
        objectDemo2.t="hello";
        objectDemo2.show("dajiahao");
        String string=objectDemo2.getT();

        Demo2<Integer> objectDemo21 = new Demo2<>();
        objectDemo21.t=123;
        objectDemo21.show(200);
        Integer integer=objectDemo21.getT();

        //泛型方法调用
        //泛型方法被调用的时候，不需要指明泛型类型。
        Demo3 demo3 = new Demo3();
        demo3.show("hello");
        demo3.show(200);
    }
}
