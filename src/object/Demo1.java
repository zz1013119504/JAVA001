package object;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class Demo1 {
    public static void main(String[] args) {
        ArrayList<Object> obj = new ArrayList<>();
        obj.add("苹果");
        obj.add("香蕉");
        obj.add("菠萝");
        System.out.println(obj.size());
        System.out.println(obj);

        //删除元素
//        obj.remove("香蕉");
//        System.out.println(obj.size());

        //遍历
        for (Object obj1 :
                obj) {
            System.out.println(obj1);
        }

        //使用迭代器（专门用来遍历集合的一种方式）
        //hasNext：是否有下一个元素
        //next:获取下一个元素
        //remove:删除当前元素
        Iterator<Object> iterator = obj.iterator();
        while (iterator.hasNext()){
            String object=(String)iterator.next();
            System.out.println(object);
        }

        //判断集合中是否包含某种元素
        System.out.println(obj.contains("苹果"));

        //for
        for (int i = 0; i < obj.size(); i++) {
            System.out.println(obj.get(i));
        }

        //ListItertor
        ListIterator listIterator=obj.listIterator();
        while (listIterator.hasNext()){
            System.out.println(listIterator.next());
        }
        while (listIterator.hasPrevious()){
            System.out.println(listIterator.previous());
        }

        //判断
        System.out.println(obj.contains("苹果"));

        //获取元素下标
        System.out.println(obj.indexOf("香蕉"));

    }
}
