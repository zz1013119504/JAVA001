package object;

import java.util.SplittableRandom;

/**
 * 泛型接口
 * 语法：接口名<T>
 * 注意：不能泛型里写静态常量
 * @param <T>
 */
public interface Myinterface<T> {
    String name="张三";

    T sever(T t);
}
