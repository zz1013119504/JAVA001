package object;

import org.omg.CORBA.PUBLIC_MEMBER;

/**
 *泛型类
 * 语法：类名<T>
 * T表示类型占位符，表示一种引用类型，如果编写多个使用逗号隔开。
 * @param <T>
 */
public class Demo2<T> {
    /**
     *
     */
    //创建变量
    T t;

    //作为方法的参数
    public void show(T t){
        System.out.println(t);
    }

    //作为方法的返回值
    public T getT(){
        return t;
    }
}
