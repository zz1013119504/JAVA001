package object;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Demo4 {
    public static void main(String[] args) {
        //创建Map集合
        HashMap<String, String> map = new HashMap<>();
        //添加元素
        map.put("1","a");
        map.put("2","b");
        map.put("3","c");

        //删除remove

        //遍历
        //使用keySet遍历
        Set<String> keyset = map.keySet();
        for (String key :
                keyset) {
            System.out.println(map.get(key));
        }

        //使用enterSet方法遍历
        Set<Map.Entry<String, String>> entries = map.entrySet();
        for (Map.Entry<String, String> entry :
                entries) {
            System.out.println(entry.getKey()+"*--*"+entry.getValue());

        }

        //判断
        System.out.println(map.containsKey("1"));
    }
}
