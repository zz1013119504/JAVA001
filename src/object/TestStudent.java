package object;

import java.util.Arrays;
import java.util.Calendar;

public class TestStudent {
    public static void main(String[] args) {
        Student xh = new Student("xh", 20);
        Student xz = new Student("xz", 22);

        //使用getClass方法来判断两个实例是否是同一个类
        Class<? extends Student> xhClass = xh.getClass();
        Class<? extends Student> xzClass = xz.getClass();

        if (xhClass==xzClass){
            System.out.println("他两是同一个类");
        }

        //hashCode方法
        System.out.println(xh.hashCode());
        System.out.println(xz.hashCode());

        //toString
        System.out.println(xh.toString());

        //equals
        System.out.println(xh.equals(xz));

        //自动装箱和拆箱
        int age=30;
        //装箱
        Integer integer=age;
        //拆箱
        int age2=integer;

        //字符串转基本类型
        String str="150";
        //使用Integer.parsexxx；
        int n2=Integer.parseInt(str);
        System.out.println(n2);

        //布尔类型字符串形式转成基本类型：“true”-----》true，非“true”——————》false
        String str2="true";
        boolean b1=Boolean.parseBoolean(str2);
        System.out.println(b1);

        String name="123";
        name="zs";
        String name2="zs";
        System.out.println(name.hashCode()==name2.hashCode());


        String content="java是世界上最好的语言。";
        System.out.println(content.length());
        System.out.println(content.charAt(3));
        System.out.println(content.contains("世界"));

        System.out.println(Arrays.toString(content.toCharArray()));
        System.out.println(content.indexOf("a"));
        System.out.println(content.lastIndexOf("a"));

        System.out.println(content.toUpperCase());

        System.out.println(content.replace("好","good"));

        Calendar ca = Calendar.getInstance();
        System.out.println(ca.getTimeInMillis());

        System.out.println(ca.get(Calendar.YEAR));

//        //arraycopy:数组的复制
//        //src：源数组
//        //srcPos：从哪个位置开始复制，下标位置
//        //dest：目标数组
//        //destPos：目标数组的位置，复制结束的位置。
//        //length：复制的长度
//        System.arraycopy(src, srcPos, dest, destPos, length);
    }
}
