package base;

public class Demo002 {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;

        //字符串拼接   +，String
        System.out.println("" + a + b);
        System.out.println(a + b + "");

        int score = 80;
        String type = score < 60 ? "不及格" : "及格";
        System.out.println(type);

    }
}
