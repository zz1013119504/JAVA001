package base;

public class Demo001 {

    //类变量static
    //类变量是不需要实例化类对象也可以使用的变量。
    static double salary=2500;

    //实例变量：从属于对象；如果不自行初始化，这个类型的默认值为0,0.0,布尔值默认为false，其余的默认值都是null。
    //实例变量就是需要实例化类之后才能使用的变量，也就是要new一个对象来使用该变量。
    String name;
    int age;

    //常量：从定义开始就已经写定的变量，后期无法被改变
    static final double pi=3.14;

    public static void main(String[] args) {
        //局部变量必须声明和初始化
        int i=10;
        System.out.println(i);

        //变量类型 变量名字=new 类名（）
        Demo001 d1=new Demo001();
        d1.name="张三";
        System.out.println(d1.name);

        //类变量可以直接使用
        System.out.println(salary);

        //常量编写
        System.out.println(pi);

        //幂运算
        double pow=Math.pow(3,2);
        System.out.println(pow);
    }
}
