package com.cn.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestJdbcUtils {
    public static void main(String[] args){
        Connection connection =null;
        Statement statement =null;
        ResultSet resultSet =null;

        try {
            connection = JdbcUtils.getConnection();
            statement = connection.createStatement();
            String sql="select * from t4 limit 10";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                System.out.println(resultSet.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.release(connection,statement,resultSet);
        }
    }
}
