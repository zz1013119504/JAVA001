package com.cn.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestPraJdbcUtils {
    public static void main(String[] args) {

        Connection connection =null;
        PreparedStatement preparedStatement=null;//防止sql注入
        ResultSet resultSet=null;

        try {
            connection = JdbcUtils.getConnection();

            //使用？占位符表示参数
            String sql="select * from t4 limit ?";
            //预编译sql，先写sql不执行。
            preparedStatement = connection.prepareStatement(sql);
            //手动添加参数
            preparedStatement.setInt(1,1);

            resultSet=preparedStatement.executeQuery();

            while (resultSet.next()){
                System.out.println(resultSet.getString("imsi"));
            }



        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.release(connection,preparedStatement,resultSet);
        }

    }
}
