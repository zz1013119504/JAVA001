package Jvm;

import java.util.ArrayList;

// -Xms:设置初始化内存分配大小，默认1/64。
// -Xmx：设置最大分配内存，默认1/4。
// -XX:+PrintGCDetails:GC清理的垃圾信息
// -XX:+HeapDumpOnOutOfMemoryError：打印堆溢出或者OOM DUMP信息

//-Xms1m -Xmx8m -XX:+HeapDumpOnOutOfMemoryError
public class Dome01 {
    byte[] array=new byte[1*1024*1024];

    public static void main(String[] args) {
        ArrayList<Dome01> list = new ArrayList<>();
        int count=0;
        try {
            while (true){
                list.add(new Dome01());
                count+=1;
            }
        } catch (Error e) {
            System.out.println("count:"+count);
            e.printStackTrace();
        }
    }
}
