package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Test01 {
}

//定义一个注解
//@Target表示注解适用的范围
@Target(value = {ElementType.METHOD,ElementType.TYPE})

//@Retention 表示注解在什么地方有效
@Retention(value = RetentionPolicy.RUNTIME)
@interface MyAnnotation{

}
