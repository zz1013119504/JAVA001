package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Test03 {

    @MyAnnotation2(name = "小红",age = 18)
    public void test() {

    }
}

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
@interface MyAnnotation2{
    //注解的参数：参数类型+参数名（）；
    String name() default "";
    int age() default 0;
    int id() default -1;
}