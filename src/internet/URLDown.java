package internet;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLDown {
    public static void main(String[] args) throws Exception {
        //1.下载地址
        URL url = new URL("https://m801.music.126.net/20211103160442/ea886a2c1e089eb316fabca6cd4b2afd/jdyyaac/obj/w5rDlsOJwrLDjj7CmsOj/11438886304/10f7/43f8/1883/d36c4fa772f269128f8f572ab74c235f.m4a");

        //2.连接到这个资源HTTP
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        InputStream inputStream=urlConnection.getInputStream();

        FileOutputStream fos = new FileOutputStream("f.m4a");

        byte[] buffer =new byte[1024];
        int len;
        while ((len=inputStream.read(buffer))!=-1){
            fos.write(buffer,0,len);//写出这个数据
        }
        fos.close();
        inputStream.close();
        urlConnection.disconnect();//断开连接
    }
}
