package internet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Tcps {
    public static void main(String[] args) {
        Socket accept =null;
        InputStream inputStream =null;
        ByteArrayOutputStream baos =null;
        try {
            //首先创建一个地址
            ServerSocket serverSocket = new ServerSocket(9999);
            while (true){
                //等待客户端连接过来
                accept = serverSocket.accept();
                //读取客户端的消息
                inputStream = accept.getInputStream();

                //管道流
                baos = new ByteArrayOutputStream();
                byte[] buffer=new byte[1024];
                int len;
                while ((len=inputStream.read(buffer))!=-1){
                    baos.write(buffer,0,len);
                }
                System.out.println(baos.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (baos!=null){
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (accept!=null){
                try {
                    accept.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
