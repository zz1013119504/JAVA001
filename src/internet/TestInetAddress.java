package internet;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class TestInetAddress {
    public static void main(String[] args) {
        try {
            //查询本机地址
            InetAddress localhost = InetAddress.getByName("localhost");
            System.out.println(localhost);
            //查询网络ip地址
            InetAddress in = InetAddress.getByName("www.baidu.com");
            System.out.println(in);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
