package internet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServerDemo02 {
    public static void main(String[] args) {
        try {
            //创建一个服务端
            ServerSocket serverSocket = new ServerSocket(9000);

            //监听客户端的链接
            Socket accept = serverSocket.accept();

            //获取输入流
            InputStream inputStream = accept.getInputStream();

            //文件输出
            FileOutputStream fileOutputStream = new FileOutputStream(new File("11.jpg"));
            byte[] bytes = new byte[1024];
            int count=0;
            while ((count=inputStream.read(bytes))!=-1){
                fileOutputStream.write(bytes,0,count);
            }

            //通知客户端我接受完毕了
            OutputStream outputStream = accept.getOutputStream();
            outputStream.write("传输完毕，可以断开".getBytes());

            //关闭资源
            fileOutputStream.close();
            inputStream.close();
            accept.close();
            serverSocket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
