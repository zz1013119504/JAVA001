package internet;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Tcpc {
    public static void main(String[] args) {
        Socket socket =null;
        OutputStream outputStream =null;
        try {
            //要知道服务器的地址和端口号
            InetAddress sip = InetAddress.getByName("127.0.0.1");
            int port=9999;

            //创建一个socket连接
            socket = new Socket(sip, port);

            //发送消息IO流
            outputStream = socket.getOutputStream();
            outputStream.write("你好啊，小老弟".getBytes());

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (outputStream!=null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
