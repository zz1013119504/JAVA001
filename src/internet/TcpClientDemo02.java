package internet;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class TcpClientDemo02 {

    public static void main(String[] args) {

        try {
            //创建一个Socket链接
            Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), 9000);

            //创建一个输出流
            OutputStream os = socket.getOutputStream();

            //读入文件
            FileInputStream fs = new FileInputStream(new File("1.jpg"));

            //写出文件
            byte[] bytes = new byte[1024];
            int count=0;
            while ((count=fs.read(bytes))!=-1){
                os.write(bytes,0,count);
            }

            //告知服务器，我已经结束了
            socket.shutdownOutput();//我已经传输完了

            //确定服务器接收完毕，才能断开连接
            InputStream inputStream = socket.getInputStream();
            //创建一个管道流
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            byte[] bytes1 = new byte[1024];
            int count1=0;
            while ((count1=inputStream.read(bytes1))!=-1){
                byteArrayOutputStream.write(bytes1,0,count1);
            }
            System.out.println(byteArrayOutputStream);

            //关闭资源
            fs.close();
            os.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
