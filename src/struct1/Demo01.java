package struct1;

import java.util.Scanner;

public class Demo01 {
    public static void main(String[] args) {
//        Scanner scanner=new Scanner(System.in);
//        System.out.println("亲输入内容:");
////        String str=scanner.nextLine();
//
////        //输入数据等于“hello”是输出该数据。
////        if (str.equals("hello")){
////            System.out.println(str);
////        }
////
////        System.out.println("end!");
//
//
//        //输入数据大于等于60就是及格，否则就是不及格。
//        double s=scanner.nextDouble();
//        if (s>=60 && s<=100){
//            System.out.println("及格");
//        }else if (s<60 && s>=0){
//            System.out.println("不及格");
//        }else {
//            System.out.println("输入错误");
//        }
//
//        scanner.close();

//        String grade = "C";
//        switch (grade) {
//            case "A":
//                System.out.println("优秀");
//                break;//如果此处不加break就会存在case穿透现象将之后的所有case的输出都输出。
//            case "B":
//                System.out.println("良好");
//                break;
//            case "C":
//                System.out.println("中");
//                break;
//            case "D":
//                System.out.println("及格");
//                break;
//            case "E":
//                System.out.println("差");
//                break;
//            default:
//                System.out.println("其他");
//                break;
//
//        }

//        int i=0;
//        while(i<100){
//            System.out.println(++i);
//        }
//
//        //循环体至少执行一次
//        int j=1000;
//        do {
//            System.out.println(++j);
//        }while (j<100);

        int oddsum = 0;
        int evensum = 0;
        for (int i = 0; i <= 100; i++) {
            if (i % 2 == 0) {
                oddsum += i;
            } else if (i % 2 == 1) {
                evensum += i;
            }
        }
        System.out.println("偶数和：" + oddsum + "       奇数和：" + evensum);

        //for循环快捷方式100.for，回车。
        for (int i = 0; i < 100; i++) {

        }

        int j = 1;
        for (int i = 0; i <= 1000; i++) {
            if (i % 5 == 0) {
                if (j < 3) {
                    j++;
                    System.out.print(i + " ");
                } else {
                    j = 1;
                    System.out.println(i + " ");
                }
            }
        }


        //九九乘法表
        for (int i=1;i<=9;i++){
            for (int jj=1;jj<=i;jj++){
                System.out.print(jj+"×"+i+"="+i*jj+"  ");
            }
            System.out.println();
        }

        //增强for循环
        int numbers[]={10,20,30,40,50};

        for (int ii:numbers){
            System.out.println(ii);
        }

        //goto在java中的标签使用
        int count=0;
        outer:for (int i=101;i<150;i++){
            for (int jj=2;jj<i/2;jj++){
                if (i%jj==0){
                    continue outer;
                }
            }
            System.out.print(i+" ");
        }

        //打印三角形
        System.out.println();
        for (int i = 1; i <= 5; i++) {
            for (int jj=5;jj>=i;jj--){
                System.out.print(" ");
            }
            for (int jj=1;jj<=i;jj++){
                System.out.print("*");
            }
            for (int jj=1;jj<i;jj++){
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
