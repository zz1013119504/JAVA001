package oop;

public class Demo {
    public static void main(String[] args) {
        int a =1;
        //生成类的快捷键，ctrl+alt+v
        Person person = new Person();

        System.out.println(a+person.name);
        change(a,person);
        System.out.println(a+person.name);
    }

    //因为返回值为空。其实个人觉得原因是因为方法中的a和主程序中的
    //a虽然都是a，但其实不是一个a，所以主程序中的a并没有被方法中的
    //a的赋值语句修改。
    public static void change(int a,Person person){
        a=10;
        //person是一个对象，具体到了人，所以可以改变其属性：引用传递
        person.name="张三";
    }


}

//定义一个Person类，有一个属性：name
class Person{
    String name;
}
