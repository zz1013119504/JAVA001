package oop.demo06;

public class APP {

    public static void main(String[] args) {
        Student student = new Student();
        Person student1 = new Student();
        Object student2 = new Student();
        Student student3 = new Student();

        System.out.println(student.hashCode());
        System.out.println(student1.hashCode());
        System.out.println(student2.hashCode());
        System.out.println(student3.hashCode());

        student1.say();
        student.say();
        student.drop();
        ((Student)student1).drop();

        System.out.println(student instanceof Student);
    }
}
