package oop.demo04;

public class Student extends Person{

    protected String name="lisi";

    public void say(){
        System.out.println("Student说了一句话！");
    }

    public void say2(String name){
        System.out.println(name);
        System.out.println(this.name);
        System.out.println(super.name);
    }

    public Student(){
        super();
        System.out.println("Student");
    }
}
