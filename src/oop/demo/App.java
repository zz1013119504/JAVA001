package oop.demo;

public class App {
    public static void main(String[] args) {
        Student xiaoming = new Student();
        Student xiaohong = new Student();

        xiaoming.name="小明";
        xiaoming.age=10;
        xiaoming.study();

        xiaoming.setName2("xiaohong");
        System.out.println(xiaoming.getName2());

        xiaohong.say();

        //Studet可以调用的方法都是自己的或者继承父类的。
        Student s1 = new Student();
        //Person可以指向子类，但是不能调用子类独有的方法。
        Person s2 = new Student();

        //对象能执行哪些方法，主要看对象左边的类型，和右边关系不大！
        ((Student)s2).song();//子类重写了父类的方法，执行子类的方法。
        s1.song();
    }
}
