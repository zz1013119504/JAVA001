package oop.demo;

public class Student extends Person{
    //属性
    String name;
    int age;

    //将name2，age2进行了私有化，在另外一边不能只用使用，要使用对应的get，set方法。
    private String name2;
    private int age2;

    public void song(){
        System.out.println("song");
    }

    public void setName2(String name2) {
        this.name2 = name2;

    }

    public void setAge2(int age2) {
        this.age2 = age2;
    }

    public String getName2() {
        return name2;
    }

    public int getAge2() {
        return age2;
    }

    //方法
    public void study(){
        System.out.println(this.name+"在学习!");
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    //1. 使用new关键字，本质是在调用构造器
    //2. 用来初始化值
    public Student() {
    }

    //有参构造：一旦定义了有参构造，无参就必须显示定义
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
