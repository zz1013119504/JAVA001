package oop.demo07;

public class Person {

    {
        System.out.println("匿名代码块");
    }

    static {
        System.out.println("静态代码块");
    }//静态代码块指挥执行一次。

    public Person() {
        System.out.println("无参构造方法");
    }

    public static void main(String[] args) {
        new Person();
        System.out.println("**************");
        new Person();
    }
}
