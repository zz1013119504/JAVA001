package oop.demo10;

public class APP {
    public static void main(String[] args) {
        Outer outer = new Outer();


        try {
            Outer.Inner inner = outer.new Inner();
            inner.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
