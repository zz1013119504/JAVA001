package zhujie;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//自定义注解
public class Test005 {
    //注解可以不显示赋值，如果没有默认值，我们就必须给注解赋值。
    @MyAnnotation2(name = "张三",school = {"zhangsan","1234"})
    public void test(){

    }

    @MyAnnotation3("xiaowang")
    public void test02(){

    }
}

@Target(value = {ElementType.METHOD,ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
@interface MyAnnotation2{
    //注解的参数：参数类型+参数名（）；
    String name() default "";//default代表默认值
    int age() default 0;
    int id() default -1;//如果默认值为-1 。代表不存在。
    String[] school();
}

@Target(value = {ElementType.TYPE,ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
@interface MyAnnotation3{
    //如果注解内只有一个参数。这可以使用value，以便上面可以不用写明名字
    String value();
}