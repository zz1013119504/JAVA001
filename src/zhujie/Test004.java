package zhujie;

import java.lang.annotation.*;

//测试元注解
@MyAnnotation1
public class Test004 {


    public void test(){

    }
}

//定义一个注解
//Target:表示我们的注解可以用在哪些地方。
@Target(value = {ElementType.METHOD,ElementType.TYPE})
//Retention：表示我们的注解在什么时候是有效的。
//runtime>class>sources
@Retention(value = RetentionPolicy.RUNTIME)//自定义的注解一般都用runtime

//表示是否将我们的注解生成在JAVAdoc中
@Documented

//子类可以继承父类的注解
@Inherited

@interface MyAnnotation1{

}