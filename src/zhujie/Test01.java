package zhujie;

public class Test01 extends Object {

    //Override 重写方法的注解
    @Override
    public String toString() {
        return super.toString();
    }

    //Deprecated：表示不推荐程序员使用，但是可以使用，或者有更好的方法
    @Deprecated
    public static void test(){}

    public static void main(String[] args) {
        System.out.println("111111");
    }
}
