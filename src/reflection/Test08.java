package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test08 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
        Class<?> c1 = Class.forName("reflection.User");

        //获得类的名字
        System.out.println(c1.getName());//获得包名+类名
        System.out.println(c1.getSimpleName());//获得类名

        //获得类的属性
        System.out.println("============================");
        Field[] declaredFields = c1.getDeclaredFields();//getFields只能找到public属性，而getDeclaredFields可以拿到全部属性。
        for (Field field:declaredFields){
            System.out.println(field);
        }

        //或得指定属性的值
        Field name = c1.getDeclaredField("name");
        System.out.println(name);

        //获得类的方法
        System.out.println("==============================");
        Method[] methods = c1.getMethods();//获得本类及其父亲类的全部public方法
        for (Method method:methods){
            System.out.println(method);
        }
        System.out.println("***************************");
        Method[] declaredMethods = c1.getDeclaredMethods();//获得本类的所有方法
        for (Method declaredMethod:declaredMethods
             ) {
            System.out.println(declaredMethod);
        }

        //获取指定方法
        //为什么下面的需要参数，因为为了避免方法重载后导致存在相同方法名，而不知道该输出哪个方法
        Method getName = c1.getMethod("getName", null);
        Method setName = c1.getMethod("setName", String.class);
        System.out.println(getName);
        System.out.println(setName);

        //获得构造器
        System.out.println("======================");
        Constructor<?>[] constructors = c1.getConstructors();
        Constructor<?>[] declaredConstructors = c1.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
        }

        for (Constructor<?> declaredConstructor : declaredConstructors) {
            System.out.println(declaredConstructor);
        }

        //获得指定构造器
        Constructor<?> constructor = c1.getConstructor(String.class, int.class, int.class);
        System.out.println(constructor);

    }
}
