package reflection;

//测试类什么时候会初始化
public class Test06 {
    static {
        System.out.println("main类被加载");
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //1.主动引用
//        Son son = new Son();

        //2. 反射也会产生主动引用
//        Class.forName("reflection.Son");

        //不会产生类的引用的方法
//        System.out.println(Son.b);//子类调用父类的静态的方法或者静态的变量，并不会让子类进行加载
//        Son[] sons = new Son[5];

        //调用常量既不会初始化父类，也不会初始化子类
        System.out.println(Son.M);
    }
}

class Father{

    static int b=6;
    static {
        System.out.println("父类被加载了");
    }
}

class Son extends Father{
    static {
        System.out.println("子类被加载了");
        m=200;
    }
    static int m=100;
    static final int M=1;
}
