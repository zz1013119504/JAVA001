package reflection;

import javax.jws.soap.SOAPBinding;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

//通过反射获取泛型
public class Test11 {

    public void test01(Map<String,User> map, List<User> list){
            System.out.println("test01");
    }

    public Map<String, User> test02(){
        System.out.println("test02");
        return null;
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Method method = Test11.class.getMethod("test01", Map.class, List.class);

        //获得泛型的返回值信息
        Type[] genericReturnType = method.getGenericParameterTypes();
        for (Type type : genericReturnType) {
            System.out.println(type);
            if (type instanceof ParameterizedType){//判断是否是结构化参数类型
                Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                for (Type actualTypeArgument : actualTypeArguments) {
                    System.out.println(actualTypeArgument);
                }
            }
        }

        System.out.println("*************************************");
        Method test02 = Test11.class.getMethod("test02", null);
        Type genericReturnType1 = test02.getGenericReturnType();
        if (genericReturnType1 instanceof ParameterizedType){
            Type[] actualTypeArguments = ((ParameterizedType) genericReturnType1).getActualTypeArguments();
            for (Type actualTypeArgument : actualTypeArguments) {
                System.out.println(actualTypeArgument);
            }

        }


    }
}
