package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//分析性能问题
//结果表明，普通调用方式效率最快，如果要使用反射调用，则需要关掉检测，以提高效率。
public class Test10 {

    //普通方式调用
    public static void test01(){
        User user = new User();
        long starttime=System.currentTimeMillis();
        for (int i = 0; i < 1000000000; i++) {
            user.getName();
        }
        long endtime=System.currentTimeMillis();
        System.out.println("经过了"+(endtime-starttime)+"ms");

    }

    //反射方式调用
    public static void test02() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        User user = new User();
        Class c1 = user.getClass();
        Method getName = c1.getDeclaredMethod("getName", null);

        long starttime=System.currentTimeMillis();
        for (int i = 0; i < 1000000000; i++) {
            getName.invoke(user,null);
        }
        long endtime=System.currentTimeMillis();
        System.out.println("经过了"+(endtime-starttime)+"ms");

    }

    //反射方式调用，关闭检测
    public static void test03() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        User user = new User();
        Class c1 = user.getClass();
        Method getName = c1.getDeclaredMethod("getName", null);
        getName.setAccessible(true);

        long starttime=System.currentTimeMillis();
        for (int i = 0; i < 1000000000; i++) {
            getName.invoke(user,null);
        }
        long endtime=System.currentTimeMillis();
        System.out.println("经过了"+(endtime-starttime)+"ms");

    }


    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        test01();
        test02();
        test03();
    }
}
