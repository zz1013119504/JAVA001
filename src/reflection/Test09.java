package reflection;

import javax.jws.soap.SOAPBinding;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//动态的创建对象，通过反射
public class Test09 {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class<?> c1 = Class.forName("reflection.User");

        //使用c1反向构造一个对象，c1.newInstance()
        User user = (User) c1.newInstance();
        System.out.println(user);
        System.out.println(user.hashCode());

        User user1 = new User();
        System.out.println(user1);
        System.out.println(user1.hashCode());

        //通过构造器创建对象
        Constructor<?> declaredConstructor = c1.getDeclaredConstructor(String.class, int.class, int.class);
        User user2 = (User) declaredConstructor.newInstance("张三", 18, 20);
        System.out.println(user2);

        //通过反射调用普通方法
        //通过反射获取一个方法
        User user3 = (User) c1.newInstance();
        Method setName = c1.getDeclaredMethod("setName", String.class);

        //invoke:激活的意思
        //参数（对象，“方法的值”）
        setName.invoke(user3,"张三");
        System.out.println(user3.getName());

        //通过反射操作属性
        User user4 = (User) c1.newInstance();
        Field name = c1.getDeclaredField("name");

        //不能直接操作私有属性，我们需要关闭程序的安全检测，可以使用将属性或者方法的setAccessible（true）来实现。
        name.setAccessible(true);
        name.set(user4,"李四");
        System.out.println(user4);


    }
}
