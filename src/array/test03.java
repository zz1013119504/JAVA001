package array;

import java.util.Arrays;

public class test03 {
    public static void main(String[] args) {
        int[] a={1,2,3,1,2,3,2,32,1};

        //打印数组a
        System.out.println(Arrays.toString(a));
        printArrays(a);

        Arrays.fill(a,10);
        printArrays(a);
    }

    public static void printArrays(int[] a){
        System.out.print("["+a[0]+", ");
        for (int i = 1; i < a.length-1; i++) {
            System.out.print(a[i]+", ");
        }
        System.out.println(a[a.length-1]+"]");
    }
}
