package array;

import java.util.Arrays;

public class ArrayDemo01 {

    public static void main(String[] args) {
        int a[];//声明了一个数组
        a=new int[10];//创建了一个数组并指明数组的大小。

        int b[]=new int[10];

        //给数组赋值
        a[0]=1;
        a[1]=2;
        a[2]=3;
        a[3]=4;
        a[4]=5;
        a[5]=6;
        a[6]=7;
        a[7]=8;
        a[8]=9;
        a[9]=10;

        //计算所有元素的和
        int sum=0;
        for (int i = 0; i < a.length; i++) {
            sum+=a[i];
        }
        System.out.println(sum);

        //静态初始化：创建+赋值
        int aa[]={1,2,3,4,5,6};
        System.out.println(aa[0]);

        //动态初始化：包含默认初始化
        int bb[]=new int[10];
        bb[0]=1;

        System.out.println(bb[0]);
        System.out.println(bb[1]);

        //快捷方式：aa.for+回车
        for (int x:aa){
            System.out.println(x);
        }

        fanzhuan(aa);


        System.out.println(aa);

        //打印数组元素Arrays。toString
        System.out.println(Arrays.toString(aa));

        int aaa[]={3,2,4,5,6,1,3,3,65,7,};
        System.out.println(Arrays.toString(sort_maopao(aaa)));

    }

    public static void fanzhuan(int aa[]){
        int cc[]=new int[aa.length];
        for (int i=aa.length-1;i>=0;i--){
            cc[aa.length-i-1]=aa[i];
        }
        for (int i : cc) {
            System.out.print(i);
        }
    }

    //冒泡排序
    //比较数组中第一个数和第二数，如果第一个数大于第二个数，则交换位置。
    //每一次比较都会产生一个最大值。
    public static int[] sort_maopao(int aa[]){

        //通过标签flag来判断原始数组是否已经有序
        int flag=0;

        for (int i=0;i<aa.length;i++){
            int a=0;
            for (int j=1;j<aa.length-i;j++){
                if (aa[j-1]>aa[j]){
                    a=aa[j-1];
                    aa[j-1]=aa[j];
                    aa[j]=a;
                    flag=1;
                }
            }
            if(flag==0){
                break;
            }
        }
        return aa;
    }


}
