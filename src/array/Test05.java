package array;

import java.util.Arrays;

public class Test05 {
    public static void main(String[] args) {
        //创建一个二维数组11*11,0：没有棋子，1：黑棋，2：白棋
        int[][] arrays=new int[11][11];
        arrays[1][2]=1;
        arrays[2][3]=2;

        for (int i = 0; i < arrays.length; i++) {
            System.out.println(Arrays.toString(arrays[i]));
        }

        //首相计算二维数组中有多少个值
        //创建一个稀疏数组来保存这里面的值
        int sum=0;
        for (int i = 0; i < arrays.length; i++) {
            for (int i1 = 0; i1 < arrays[i].length; i1++) {
                if (arrays[i][i1]!=0){
                    sum+=1;
                }
            }
        }
        int[][] arrray2 =new int[sum+1][3];
        arrray2[0][0]=11;
        arrray2[0][1]=11;
        arrray2[0][2]=sum;

        int ii=1,jj=0;
        for (int i = 0; i < arrays.length; i++) {
            for (int i1 = 0; i1 < arrays[i].length; i1++) {
                if (arrays[i][i1]!=0){
                    arrray2[ii][jj]=i;
                    arrray2[ii][jj+1]=i1;
                    arrray2[ii][jj+2]=arrays[i][i1];
                    ii++;
                }
            }
        }
        for (int i = 0; i < arrray2.length; i++) {
            System.out.println(Arrays.toString(arrray2[i]));
        }

        System.out.println("还原");
        //首先创建一个数组
        //给对应位置赋值
        int[][] a3=new int[arrray2[0][0]][arrray2[0][1]];

        for (int i = 1; i < arrray2.length; i++) {
            a3[arrray2[i][0]][arrray2[i][1]]=arrray2[i][2];
        }
        for (int i = 0; i < a3.length; i++) {
            System.out.println(Arrays.toString(a3[i]));
        }
    }
}
