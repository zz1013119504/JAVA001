package array;

import java.util.Arrays;

/**
 * 冒牌排序
 */
public class Test04 {
    public static void main(String[] args) {
        int[] a = {1, 4, 2, 6, 4, 5, 3, 6, 8};

        int x = 0;
        int flag = 0;
        System.out.println(Arrays.toString(a));
        int len=a.length;
        for (int i = 0; i < len - 1; i++) {
            for (int i1 = 0; i1 < a.length-i-1; i1++) {
                if (a[i1 + 1] < a[i1]) {
                    flag = 1;
                    x = a[i1];
                    a[i1] = a[i1 + 1];
                    a[i1 + 1] = x;
                }
            }
            if (flag == 0) {
                break;
            }
        }
        System.out.println(Arrays.toString(a));
    }
}
